/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_tomting_orion_ordbc_Ordbc */

#ifndef _Included_com_tomting_orion_ordbc_Ordbc
#define _Included_com_tomting_orion_ordbc_Ordbc
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_tomting_orion_ordbc_Ordbc
 * Method:    initializeOrdbc
 * Signature: (Ljava/lang/String;III)Z
 */
JNIEXPORT jboolean JNICALL Java_com_tomting_orion_ordbc_Ordbc_initializeOrdbc
  (JNIEnv *, jobject, jstring, jint, jint, jint);

/*
 * Class:     com_tomting_orion_ordbc_Ordbc
 * Method:    destroyOrdbc
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_com_tomting_orion_ordbc_Ordbc_destroyOrdbc
  (JNIEnv *, jobject);

/*
 * Class:     com_tomting_orion_ordbc_Ordbc
 * Method:    run
 * Signature: ([BZ)[B
 */
JNIEXPORT jbyteArray JNICALL Java_com_tomting_orion_ordbc_Ordbc_run
  (JNIEnv *, jobject, jbyteArray, jboolean);

#ifdef __cplusplus
}
#endif
#endif
