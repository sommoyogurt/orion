#ifndef COREHUTL
#define COREHUTL
#include "CoreVcl_.hpp"
#include "CoreList.hpp"

class CoreHutl
{
private:

	bool static match (char *bString, char *bWild);
  bool static __fastcall getst (AnsiString sFilename,SYSTEMTIME* cWrite);	

public:

	void static __fastcall readr (	AnsiString sPath, AnsiString sMask,
																	AnsiString** sFilenames, int& iCount,
																	AnsiString sFoldermask = NULL);
	void static __fastcall readr (	AnsiString sPath, AnsiString sMask,
																	CoreList<AnsiString>& cFilenames);
	AnsiString static __fastcall appnm (void* hInstance);


};

#endif