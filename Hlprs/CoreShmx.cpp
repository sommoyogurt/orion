#include "CoreShme.hpp"
#include <boost/interprocess/xsi_shared_memory.hpp>
#include <boost/interprocess/mapped_region.hpp>


  __fastcall CoreShme::CoreShme (bool bServer, AnsiString sName, int64vcl iSize) { 

		//printf ("alloco %s %i\n", sName.c_str (), iSize);

		iVsize = iSize;
		cVsharedmemory = NULL;
		try {
			//if (bServer) cVsharedmemory = new windows_shared_memory (open_or_create, TEXT (sName.c_str ()), read_write, iVsize);
			xsi_key key(sName.c_str (), 1);
			if (bServer) cVsharedmemory = new  xsi_shared_memory(create_only, key, iVsize);
			else cVsharedmemory = new xsi_shared_memory(open_only, key);
			cVregion = boost::shared_ptr<mapped_region> (new mapped_region (*(xsi_shared_memory*)cVsharedmemory, read_write));
			bVrunning = true;
		} catch (...) {
			bVrunning = false;
			//printf ("CoreShme %lld\n", iSize);
		}
	}

  __fastcall CoreShme::~CoreShme () { 

		//printf ("dealloco %lld\n", iVsize);
		if (cVsharedmemory != NULL) delete (xsi_shared_memory*) cVsharedmemory;
	}

  /*ADDReSs*/
	void* __fastcall CoreShme::addrs () {

		return cVregion->get_address ();
	}

	/*GET SiZe*/
	int64vcl __fastcall CoreShme::getsz () {

		return iVsize;
	}

	/*ALIVE*/
	bool __fastcall CoreShme::alive () {

		return bVrunning;
	}
