#ifndef COREORN_
#define COREORN_
#include <protocol/TBinaryProtocol.h>
#include "CoreVcl_.hpp"
#include "CorePool.hpp"
#include "CorePlan.hpp"
#include "CorePeer.hpp" 

#define iCversion "1.1.00 (30 nov)"  

class CoreOrn_ {

private:
	PTR_POOL cVpool;
	PTR_PLANNER cVplanner;	
	CoreEnpo::NODEDATA cVnodedata;	

public:

  __fastcall CoreOrn_(	PTR_POOL cPool, AnsiString sPath, AnsiString sFilename, 
												CoreEnpo::iCstategossipnode iState, bool bResetlocal = false);
  __fastcall ~CoreOrn_ ();

	PTR_PLANNER __fastcall getpl ();
	void __fastcall getnd (	CoreEnpo::NODEDATA& cNodedata);
};

typedef boost::shared_ptr<CoreOrn_> PTR_ORION;

#endif